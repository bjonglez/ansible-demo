# Ansible demo

Goal is to have a simple playbook to play with parallelism.

We enabled profiling in `ansible.cfg` to get a better idea of the performance
of running a playbook.  We also enable SSH pipelining (it's not enabled by default
and can improve runtime performance by 2x).

## Setup for Grid'5000

- Start the Grid'5000 VPN or make sure you have appropriate SSH jump host configuration
- Edit `group_vars/g5k.yml` to set your G5K login name

## Run on Grid'5000

- Reserve a node, e.g. in Nantes: `oarsub -I -p ecotype`
- Modify `inventories/g5k` and put the hostname of your node
- Run Ansible with the real Docker playbook:

    ansible-playbook -i inventories/g5k docker.yml

## Run locally

We have a "fake" playbook that simply sleeps for each step. It means you can run it
locally without having to reserve a Grid'5000 node.

To run it, simply use:

    ansible-playbook -i inventories/local docker-sleep.yml
